package commands;

import helpers.ApiMessages;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import plugin.api.CommandInterface;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by niyaz on 22.11.2018.
 * <p>
 * How plugins was implemented
 * api-plugin.jar in libs folder contains interface CommandInterface
 * <p>
 * Every plugin must have class what implements interface from this jar
 * plugins are loaded from plugins folder
 */
public class CommandExecutor {

    private String commandsPath;
    private final Logger logger = LogManager.getLogger(getClass().getName());

    /**
     * @param commandsPath path where plugins are located
     */
    public CommandExecutor(String commandsPath) {
        this.commandsPath = commandsPath;
    }

    /**
     * executes command with given keyword
     * if there is two plugins wih similar keywords
     * unpredictable behavior (always wanna write this)
     *
     * @param keyword    keyword of command without '/'
     * @param parameters parameters provided by user
     * @return result of execution
     */
    public String executeCommand(String keyword, String parameters) {
        if (keyword.equals("help")) {
            return getHelp();
        }

        for (CommandInterface plugin : getPlugins()) {
            if (plugin.getKeyword().equals(keyword)) {
                return plugin.executeCommand(parameters);
            }
        }

        return "plugin was not found";
    }

    /**
     * @return list of plugins in jar files in given directory
     */
    private List<CommandInterface> getPlugins() {
        File pluginDir = new File(commandsPath);

        // loading all jars in given directory
        File[] jars = getListOfJars(pluginDir);

        ArrayList<CommandInterface> result = new ArrayList<>();


        if (jars != null && jars.length > 0) {
            Pair<ArrayList<String>, ArrayList<URL>> temp = getNamesAndURLs(jars);
            ArrayList<String> classes = temp.getKey(); // name of classes inside jar
            ArrayList<URL> urls = temp.getValue(); // for URLClassLoader constructor

            // when we got all classes inside jar we can check if some implements
            // interface and add it to result list
            try (URLClassLoader urlClassLoader = new URLClassLoader(urls.toArray(new URL[urls.size()]))) {

                for (String className : classes) {
                    try {
                        Class cls = getClassByName(className, urlClassLoader);

                        //checking presence of CommandInterface interface
                        CommandInterface tempInst = getCommand(cls);
                        if (tempInst != null) {
                            result.add(tempInst);
                        }
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                logger.error("cannot load URLs" + e.getMessage());
            }
        }
        return result;
    }

    private Class getClassByName(String className, URLClassLoader urlClassLoader) throws ClassNotFoundException {
        return urlClassLoader.loadClass(
                className.replaceAll("/", ".")
                        .replace(".class", "")
        ); // changing name of class to binary
    }

    /**
     * @return string with keywords and description of every command
     */
    private String getHelp() {
        StringBuilder result = new StringBuilder();

        for (CommandInterface inrf : getPlugins()) {
            result.append("\t").append(inrf.getKeyword()).append("\n\t")
                    .append(inrf.getShortDescription()).append("\n");
        }

        return result.toString();
    }

    /**
     * returns list of jars inside directory
     * @param pluginDir directory
     * @return
     */
    private static File[] getListOfJars(File pluginDir) {
        return pluginDir.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile() && file.getName().endsWith(".jar");
            }
        });
    }

    /**
     *
     * @param jars jar files
     * @return names and URLs of classes inside jars
     */
    private Pair<ArrayList<String>,ArrayList<URL>> getNamesAndURLs(File[] jars) {
        ArrayList<String> classes = new ArrayList<>(); // name of classes inside jar
        ArrayList<URL> urls = new ArrayList<>(jars.length); // for URLClassLoader constructor
        for (File file : jars) {
            try (JarFile jar = new JarFile(file)) { // opening file as JarFile
                Enumeration<JarEntry> entries = jar.entries(); //getting all entries in jar
                while (entries.hasMoreElements()) {
                    JarEntry jarEntry = entries.nextElement();
                    if (jarEntry.getName().endsWith(".class")) {
                        classes.add(jarEntry.getName()); // adding only classes into list
                    }
                }
            } catch (IOException e){
                logger.error("cannot open jar: " + file.getName());
            }
            URL url = null;
            try {
                url = file.toURI().toURL();
            } catch (MalformedURLException e) {
                logger.error(e.getMessage());
            }
            urls.add(url);
        }
        return new Pair<>(classes, urls);
    }

    /**
     *
     * @param cls class to check if it's implements CommandInterface
     * @return instance of CommandInterface
     */
    private CommandInterface getCommand(Class cls) {
        Class[] interfaces = cls.getInterfaces();
        for (Class intface : interfaces) {
            if (intface.equals(CommandInterface.class)) {
                try {
                    return (CommandInterface) cls.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    logger.error("instantiating CommandInterface: " + e.getMessage());
                }
            }
        }
        return null;
    }
}
