package server;

import managers.ConnectionManager;
import managers.DefaultConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Waits for new socket connection
 * when new connection is received it notifies about it
 * ConnectionManager using acceptConnection(Socket) method
 */
public class MyServer {

    private final Logger logger = LogManager.getLogger(getClass().getName());
    private static ConnectionManager connectionManager = new DefaultConnectionManager();
    private final int SERVER_PORT;
    private final String KEYWORD_STOP;
    private final InputStream readCommands;
    private ServerSocket server;


    public MyServer(int SERVER_PORT, String KEYWORD_STOP, InputStream in) {
        this.SERVER_PORT = SERVER_PORT;
        this.KEYWORD_STOP = KEYWORD_STOP;
        this.readCommands = in;
        try {
            server = new ServerSocket(SERVER_PORT);
        } catch (IOException e) {
            logger.error("server will not start because " + e.getMessage());
        }
    }

    public void start() {
        if (server != null) {
            logger.info("server started on port " + SERVER_PORT);
            startCommandsReader();
            startSocketsListener();
        }
    }

    /**
     * listens for user's input
     * executes commands
     * commands list:
     *    1) KEYWORD_STOP - stops the server
     *    that's all :)
     */
    private void startCommandsReader() {
        final BufferedReader br = new BufferedReader(new InputStreamReader(readCommands));
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                logger.info("listener for commands started");
                while (!server.isClosed()) {
                    try {
                        String serverCommand = br.readLine();
                        if (serverCommand.equalsIgnoreCase(KEYWORD_STOP)) { // stops server
                            server.close();
                            logger.info("stopping server");
                            break;
                        }
                    } catch (IOException e) {
                        logger.error("closing server socket failed, because: " + e.getMessage());
                    }
                }
            }
        });
        thread.start();
    }

    /**
     * listens for new connection
     */
    private void startSocketsListener() {
        Thread listener = new Thread(new Runnable() {
            @Override
            public void run() {
                logger.info("listener for sockets started");
                while (!server.isClosed()) {
                    try {
                        Socket client = server.accept();
                        logger.info("received new socket connection from "
                                + client.getInetAddress().getHostAddress());
                        connectionManager.acceptConnection(client);
                    } catch (IOException e) {
                        logger.error("receiving new socket: " + e.getMessage());
                    }
                }
            }
        });
        listener.start();
    }
}
