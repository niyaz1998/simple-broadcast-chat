package model.messages;

/**
 * Created by niyaz on 12.11.2018.
 */
public class RegistrationMessage extends AbstractMessage {

    private String nickname;

    public RegistrationMessage(String type) {
        super(type);
    }

    public RegistrationMessage(String type, String nickname) {
        super(type);
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
