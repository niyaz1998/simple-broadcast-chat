package model.messages;

/**
 * Represents abstract message from user of this chat
 * every message have it's type
 */
public class AbstractMessage {

    private final String type;

    public AbstractMessage(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
