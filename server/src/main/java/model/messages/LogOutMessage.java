package model.messages;

/**
 * Created by niyaz on 12.11.2018.
 */
public class LogOutMessage extends AbstractMessage {
    public LogOutMessage(String type) {
        super(type);
    }
}
