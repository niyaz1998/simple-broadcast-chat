package model.messages;

/**
 * Created by niyaz on 12.11.2018.
 */
public class CommandMessage extends AbstractMessage {

    private String keyword, parameters;

    public CommandMessage(String type) {
        super(type);
    }

    public CommandMessage(String type, String keyword, String parameters) {
        super(type);
        this.keyword = keyword;
        this.parameters = parameters;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }
}
