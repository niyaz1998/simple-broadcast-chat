package model.messages;

/**
 * Created by niyaz on 12.11.2018.
 */
public class UsualMessage extends AbstractMessage {

    private String message;

    public UsualMessage(String type) {
        super(type);
    }

    public UsualMessage(String type, String message) {
        super(type);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
