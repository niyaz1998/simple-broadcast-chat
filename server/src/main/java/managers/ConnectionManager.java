package managers;

import java.net.Socket;

/**
 * Created by niyaz on 09.11.2018.
 */
public abstract class ConnectionManager {

    /**
     * accepts connection from new sockets
     * and manages it
     *
     * @param newClient new connection to track
     */
    public abstract void acceptConnection(Socket newClient);

    /**
     * sends messages to some address (IPv4)
     *
     * @param message message to send
     * @param address IPv4 address of client
     */
    public abstract void sendMessage(String message, String address);

    /**
     * needs to be called when client with some address disconnected
     *
     * @param address
     */
    public abstract void disconnect(String address);
}
