package managers;

import commands.CommandExecutor;
import helpers.ApiMessages;
import helpers.DateHelper;
import helpers.ProtocolHelper;
import model.messages.AbstractMessage;
import model.messages.CommandMessage;
import model.messages.RegistrationMessage;
import model.messages.UsualMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by niyaz on 09.11.2018.
 */
public class DefaultClientManager extends ClientsManager {

    private final Logger logger = LogManager.getLogger(getClass().getName());
    private HashMap<String, String> addressToNickname = new HashMap<>(2); // maps address (or id) of socket to his nickname
    private HashSet<String> nicknamesSet = new HashSet<>(2); //set of nickname so manager can track names
    private CommandExecutor commandExecutor = new CommandExecutor("C:\\Users\\mvideo\\" +
            "IdeaProjects\\Industrial Java Programming\\simple-broadcast-chat\\server\\plugins");
    private ConnectionManager connectionManager; // connection manager for sending messages

    private final Object lock;

    public DefaultClientManager(ConnectionManager connectionManager, Object lock) {
        this.connectionManager = connectionManager;
        this.lock = lock;
    }

    @Override
    public void acceptMessage(String jsonMessage, String address) {
        logger.info("accepting message from address: " + address + " message: " + jsonMessage);
        AbstractMessage parsedMessage = ProtocolHelper.parseMessageFromUser(jsonMessage);

        if (parsedMessage.getType().equals(ProtocolHelper.TYPE_WITHOUT_TYPE)) {
            return; // message was not parsed
        }

        switch (parsedMessage.getType()) {
            case ProtocolHelper.TYPE_REGISTRATION:
                registerUser((RegistrationMessage) parsedMessage, address);
                break;
            case ProtocolHelper.TYPE_SIMPLE_MESSAGE:
                processUsualMessage((UsualMessage) parsedMessage, address);
                break;
            case ProtocolHelper.TYPE_COMMAND:
                executeCommand((CommandMessage) parsedMessage, address);
                break;
        }
    }

    /**
     * executes command provided by user
     * in new thread
     * so thread responsible for user will not stop
     * if command requires time
     * @param parsedMessage command with keyword and parameters
     * @param address id of user
     */
    private void executeCommand(final CommandMessage parsedMessage, final String address) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String message = ProtocolHelper.commandToJson(
                        parsedMessage.getKeyword(), parsedMessage.getParameters(),
                        commandExecutor.executeCommand(
                                parsedMessage.getKeyword(), parsedMessage.getParameters())
                , DateHelper.now());

                connectionManager.sendMessage(message, address);
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * disconnects user
     * @param address id of user
     */
    @Override
    public void disconnect(String address) {
        String jsonMessage;
        synchronized (lock) {
            logger.info("disconnecting address: " + address);
            if (address == null)
                return;
            connectionManager.disconnect(address); // disconnect on connection manager
            jsonMessage = ProtocolHelper.logOutToJson(
                    addressToNickname.get(address),
                    DateHelper.now());
            nicknamesSet.remove(addressToNickname.get(address)); // remove from nickname set, so his name can be used now
            addressToNickname.remove(address); // remove id of user
        }
        sendAll(jsonMessage); // sending message what user leaved chat
    }

    /**
     * receives message from user
     * and then sends it to all users
     * @param parsedMessage usual message from user
     * @param address id of user
     */
    private void processUsualMessage(UsualMessage parsedMessage, String address) {
        synchronized (lock) {
            String senderNick = addressToNickname.get(address);
            if (senderNick == null)
                return;
            String jsonMessage = ProtocolHelper.messageToJson(
                    parsedMessage.getMessage(),
                    addressToNickname.get(address),
                    DateHelper.now());
            sendAll(jsonMessage);
        }
    }

    /**
     * sends to all users new message
     * in new thread
     * @param jsonMessage message to send
     */
    private void sendAll(String jsonMessage) {
        synchronized (lock) {
            SendAllRunnable runnable = new SendAllRunnable(
                    connectionManager, addressToNickname.keySet(), jsonMessage);

            new Thread(runnable).start();
        }
    }

    /**
     * simply speaking we set nickname to some address (id)
     * @param parsedMessage message with nickname of user
     * @param address id of user
     */
    private void registerUser(RegistrationMessage parsedMessage, String address) {
        String nick;
        synchronized (lock) {
            logger.info("registration new address: " + address);
            nick = parsedMessage.getNickname();
            if (nick == null)
                return;
            if (nick.length() == 0)
                connectionManager.sendMessage(ProtocolHelper.errorToJson(ApiMessages.INVALID_DATA), address);
            if (nicknamesSet.contains(nick)) {
                connectionManager.sendMessage(
                        ProtocolHelper.errorToJson(ApiMessages.USER_EXISTS), address);
                return;
            }

            nicknamesSet.remove(addressToNickname.get(address)); // remove old nick, if exists
            nicknamesSet.add(nick); // add new one
            addressToNickname.put(address, nick);
            connectionManager.sendMessage(
                    ProtocolHelper.registrationToJson(nick, DateHelper.now()), address); // notify user what he registered successfully
        }
        sendAll(ProtocolHelper.newUserToJson(nick, DateHelper.now())); // notify all users about new user
    }

    /**
     * runnable what allows to send messages to all users in new thread
     */
    private class SendAllRunnable implements Runnable {

        private ConnectionManager connectionManager;
        private Set<String> addresses;
        private String message;

        public SendAllRunnable(final ConnectionManager connectionManager, final Set<String> addresses, final String message) {
            this.connectionManager = connectionManager;
            this.addresses = addresses;
            this.message = message;
        }

        @Override
        public void run() {
            for (String address : addresses) {
                connectionManager.sendMessage(message, address);
            }
        }
    }
}
