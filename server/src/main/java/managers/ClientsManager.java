package managers;

/**
 * manages clients
 * their nicknames
 * resend message to other users
 * handles commands
 */
public abstract class ClientsManager {

    /**
     * @param newMessage
     * @param address
     */
    public abstract void acceptMessage(String newMessage, String address);


    /**
     * needs to be called when client with some address disconnected
     *
     * @param address
     */
    public abstract void disconnect(String address);
}
