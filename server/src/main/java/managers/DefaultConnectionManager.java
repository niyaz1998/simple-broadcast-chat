package managers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import runnables.ChatRunnable;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by niyaz on 09.11.2018.
 */
public class DefaultConnectionManager extends ConnectionManager {

    private final Logger logger = LogManager.getLogger(getClass().getName());

    private Map<String, Socket> chatConnections = new HashMap<>(2);;
    private ClientsManager clientsManager;

    private final Object lock = new Object(); // for synchronized blocks

    public DefaultConnectionManager() {
        clientsManager = new DefaultClientManager(this, lock);
    }

    @Override
    public void acceptConnection(Socket newClient) {
        Thread thread;
        try {
            thread = new Thread(
                    new ChatRunnable(
                            newClient.getInputStream(),
                            newClient.getInetAddress().getHostAddress(),
                            clientsManager));
            synchronized (lock) {
                chatConnections.put(newClient.getInetAddress().getHostAddress(), newClient);
            }
            thread.setDaemon(true);
            thread.start();

        } catch (IOException e) {
            logger.error("accepting new socket " + newClient.getInetAddress().getHostAddress()
                    + "error in getting InputStream");
        }
    }

    @Override
    public void sendMessage(final String message, final String address) {
        synchronized (lock) {
            if (chatConnections.get(address) == null)
                return;
            writeMessageToSocket(message, chatConnections.get(address));
        }
    }

    @Override
    public void disconnect(String address) {
        synchronized (lock) {
            if (address == null)
                return;
            try {
                chatConnections.get(address).close();
            } catch (IOException e) {
                logger.error("closing socket for address: " + address + "\nerror: " + e.getMessage());
            }
            chatConnections.remove(address);
        }
    }

    /**
     * writes message to socket
     * @param jsonMessage message to send
     * @param currentSocket socket send to
     */
    private void writeMessageToSocket(String jsonMessage, Socket currentSocket) {
        logger.info("writing: " + jsonMessage + " to: " + currentSocket.getInetAddress().getHostAddress());
        try {
            DataOutputStream oos = new DataOutputStream(currentSocket.getOutputStream());
            oos.write((jsonMessage + "\n").getBytes());
        } catch (IOException e) {
            logger.error("IO exception: " + e.getMessage()
                    + " occurred while sending " + jsonMessage
                    + " to user " + currentSocket.getInetAddress().getHostAddress()
            );
        }
    }
}
