package helpers;

/**
 * Created by niyaz on 09.11.2018.
 */
public class ApiMessages {

    /**
     * contains some default messages that can be sent to client
     * or may be not
     * who knows
     */
    public final static String USER_EXISTS = "user with this name already exists";
    public final static String EMPTY_MESSAGE = "message is empty";
    public final static String INVALID_DATA = "you sent invalid data";

}
