package helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by niyaz on 12.11.2018.
 */
public class DateHelper {

    /**
     * @return time in format yyyyMMdd'T'HHmmss
     */
    public static String now() {
        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        return format.format(now);
    }
}
