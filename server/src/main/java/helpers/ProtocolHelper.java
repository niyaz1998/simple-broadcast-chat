package helpers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import model.messages.*;

import java.lang.reflect.Type;
import java.sql.Time;
import java.util.Map;

/**
 * Class helps to transform strings into messages according to protocol
 * you can find protocol in directory "simple-broadcast-chat"
 */
public class ProtocolHelper {


    public static final String TYPE = "type"; // field included in every message
    public static final String TYPE_REGISTRATION = "REGISTRATION";
    public static final String TYPE_LOGOUT = "LOGOUT";
    public static final String TYPE_COMMAND = "COMMAND";
    public static final String TYPE_SIMPLE_MESSAGE = "SIMPLE_MESSAGE";
    public static final String TYPE_UNSUPPORTED = "UNSUPPORTED_TYPE";
    public static final String TYPE_WITHOUT_TYPE = "WITHOUT_TYPE";
    public static final String TYPE_ERROR = "ERROR";
    public static final String TYPE_NEW_USER = "NEW_USER";

    public static final String NICKNAME = "nickname";
    public static final String MESSAGE = "message";
    public static final String KEYWORD = "keyword";
    public static final String PARAMETERS = "parameters";
    public static final String SENDER = "sender";
    public static final String TIME = "time";
    public static final String RESULT = "result";
    public static final String DESCRIPTION = "description";


    private static final Gson gson = new Gson();

    public static String registrationToJson(String nickname, String time) {
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty(TYPE, TYPE_REGISTRATION);
        rootObject.addProperty(NICKNAME, nickname);
        rootObject.addProperty(TIME, time);

        return gson.toJson(rootObject);
    }

    public static String newUserToJson(String nickname, String time) {
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty(TYPE, TYPE_NEW_USER);
        rootObject.addProperty(NICKNAME, nickname);
        rootObject.addProperty(TIME, time);

        return gson.toJson(rootObject);
    }

    public static String logOutToJson(String nickname, String time) {
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty(TYPE, TYPE_LOGOUT);
        rootObject.addProperty(NICKNAME, nickname);
        rootObject.addProperty(TIME, time);

        return gson.toJson(rootObject);
    }

    public static String messageToJson(String content, String sender, String time) {
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty(TYPE, TYPE_SIMPLE_MESSAGE);
        rootObject.addProperty(MESSAGE, content);
        rootObject.addProperty(SENDER, sender);
        rootObject.addProperty(TIME, time);

        return gson.toJson(rootObject);
    }

    public static String commandToJson(String keyword, String parameters, String result, String time) {
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty(TYPE, TYPE_COMMAND);
        rootObject.addProperty(RESULT, result);
        rootObject.addProperty(KEYWORD, keyword);
        rootObject.addProperty(PARAMETERS, parameters);
        rootObject.addProperty(TIME, time);

        return gson.toJson(rootObject);
    }

    public static String errorToJson(String description) {
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty(TYPE, TYPE_ERROR);
        rootObject.addProperty(DESCRIPTION, description);

        return gson.toJson(rootObject);
    }

    /**
     * parses json messages from the user according to the protocol
     *
     * @param json JSON string to parse
     * @return map of keywords and values
     */
    public static AbstractMessage parseMessageFromUser(String json) {
        Type mapType = new TypeToken<Map>() {
        }.getType();
        Map<String, String> parsedMessage = gson.fromJson(json, mapType);

        if (parsedMessage.get(TYPE) == null || parsedMessage.get(TYPE).length() == 0) {
            return new AbstractMessage(TYPE_WITHOUT_TYPE);
        }
        switch (parsedMessage.get(TYPE)) {
            case TYPE_REGISTRATION:
                RegistrationMessage rMessage = new RegistrationMessage(TYPE_REGISTRATION);
                rMessage.setNickname(parsedMessage.get(NICKNAME));
                return rMessage;
            case TYPE_LOGOUT:
                LogOutMessage loMessage = new LogOutMessage(TYPE_LOGOUT);
                return loMessage;
            case TYPE_SIMPLE_MESSAGE:
                UsualMessage uMessage = new UsualMessage(TYPE_SIMPLE_MESSAGE);
                uMessage.setMessage(parsedMessage.get(MESSAGE));
                return uMessage;
            case TYPE_COMMAND:
                CommandMessage cMessage = new CommandMessage(TYPE_COMMAND);
                cMessage.setKeyword(parsedMessage.get(KEYWORD));
                cMessage.setParameters(parsedMessage.get(PARAMETERS));
                return cMessage;
        }
        return new AbstractMessage(TYPE_WITHOUT_TYPE);
    }
}
