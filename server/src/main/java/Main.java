import server.MyServer;

public class Main {

    public static void main(String[] args) {
        // configurations of server
        MyServer myServer = new MyServer(8080, "/exit", System.in);
        myServer.start();
    }
}