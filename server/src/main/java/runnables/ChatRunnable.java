package runnables;

import com.google.gson.Gson;
import managers.ClientsManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

/**
 * Runnable reads from input stream
 * and when it reads some valid JSON message its notifies ClientManager
 * using acceptMessage(String) function
 */
public class ChatRunnable implements Runnable {
    private final Logger logger = LogManager.getLogger(getClass().getName());
    private static final Gson gson = new Gson();

    private InputStream input;
    private String address; // id of user
    private ClientsManager clientsManager;

    public ChatRunnable(InputStream input, String address, ClientsManager clientsManager) {
        this.input = input;
        this.address = address;
        this.clientsManager = clientsManager;
    }

    @Override
    public void run() {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(input))){

            StringBuilder message = new StringBuilder();
            String newLine;

            while ((newLine = in.readLine()) != null) {
                message.append(newLine);
                if (isJSONValid(message.toString())) {
                    clientsManager.acceptMessage(message.toString(), address);
                    message = new StringBuilder();
                }
            }
        } catch (IOException e) {
            logger.error("during listening thread for "
                    + address + " " + e.getMessage());
        } finally {
            clientsManager.disconnect(address);
        }
    }


    /**
     * checks if jsonString is a valid JSON string
     * @param jsonInString
     * @return
     */
    private static boolean isJSONValid(String jsonInString) {
        try {
            gson.fromJson(jsonInString, Object.class);
            return true;
        } catch(com.google.gson.JsonSyntaxException ex) {
            return false;
        }
    }
}
