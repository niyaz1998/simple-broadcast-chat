package managers;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import static org.junit.Assert.*;

/**
 * Created by niyaz on 15.11.2018.
 */
public class DefaultConnectionManagerTest {
    @Test
    public void acceptConnection() throws Exception {
        DefaultConnectionManager connectionManager = new DefaultConnectionManager();
        ServerSocket serverSocket = new ServerSocket(8080);
        connectionManager.acceptConnection(new Socket("localhost", 8080));
        serverSocket.close();
    }

    @Test
    public void sendMessage() throws Exception {
        DefaultConnectionManager connectionManager = new DefaultConnectionManager();
        ServerSocket serverSocket = new ServerSocket(8080);
        Socket client = new Socket("localhost", 8080);
        Socket received = serverSocket.accept();
        connectionManager.acceptConnection(received);

        BufferedReader in  = new BufferedReader(new InputStreamReader(client.getInputStream()));
        connectionManager.sendMessage("message\n", "127.0.0.1");
        System.out.println(in.readLine());
        serverSocket.close();
        client.close();
        received.close();
    }

}