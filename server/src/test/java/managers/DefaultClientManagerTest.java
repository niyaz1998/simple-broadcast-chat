package managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.Test;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by niyaz on 15.11.2018.
 */
public class DefaultClientManagerTest {
    @Test
    public void clientRegistration() throws Exception {
        DefaultClientManager clientManager = new DefaultClientManager(new ConnectionManager() {
            @Override
            public void acceptConnection(Socket newClient) {
            }

            @Override
            public void sendMessage(String message, String address) {
            }

            @Override
            public void disconnect(String address) {
            }
        }, new Object());
        Field addressToNicknamef = clientManager.getClass().getDeclaredField("addressToNickname"); //NoSuchFieldException
        addressToNicknamef.setAccessible(true);
        HashMap<String, String> addressToNick = (HashMap<String, String>) addressToNicknamef.get(clientManager);
        Field nicknamesSetf = clientManager.getClass().getDeclaredField("nicknamesSet"); //NoSuchFieldException
        nicknamesSetf.setAccessible(true);
        HashSet<String> nickSet = (HashSet<String>) nicknamesSetf.get(clientManager);


        clientManager.acceptMessage("{\"type\":\"REGISTRATION\",\"nickname\":\"nick\"}", "1");

        assertEquals("nick", addressToNick.get("1"));
        assertTrue(nickSet.contains("nick"));

        clientManager.acceptMessage("{\"type\":\"REGISTRATION\",\"nickname\":\"nick2\"}", "1");
        clientManager.acceptMessage("{\"type\": \"SIMPLE_MESSAGE\",\"message\": \"content of message\"}", "1");
        clientManager.acceptMessage("{\"type\":\"COMMAND\"," +
                "\"keyword\":\"nick\",\"parameters\":\"keyword\"}", "1");

        assertEquals("nick2", addressToNick.get("1"));
        assertTrue(nickSet.contains("nick2"));
        assertFalse(nickSet.contains("nick"));

        clientManager.disconnect("1");
        assertFalse(nickSet.contains("nick"));
        assertEquals(null, addressToNick.get("1"));
    }

}