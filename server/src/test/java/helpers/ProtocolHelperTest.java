package helpers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import model.messages.*;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by niyaz on 12.11.2018.
 */
public class ProtocolHelperTest {
    @Test
    public void newUserToJson() throws Exception {
        assertEquals("{\"type\":\"REGISTRATION\",\"nickname\":\"nick\",\"time\":\"a\"}",
                ProtocolHelper.registrationToJson("nick", "a"));
    }

    @Test
    public void logOutToJson() throws Exception {
        assertEquals("{\"type\":\"LOGOUT\",\"nickname\":\"nick\",\"time\":\"a\"}",
                ProtocolHelper.logOutToJson("nick", "a"));
    }

    @Test
    public void messageToJson() throws Exception {
        assertEquals("{\"type\":\"SIMPLE_MESSAGE\",\"message\":\"message\",\"sender\":\"nick\",\"time\":\"a\"}",
                ProtocolHelper.messageToJson("message","nick", "a"));
    }


    @Test
    public void commandToJson() throws Exception {
        assertEquals("{\"type\":\"COMMAND\",\"result\":\"params\"," +
                        "\"keyword\":\"nick\",\"parameters\":\"keyword\",\"time\":\"1\"}",
                ProtocolHelper.commandToJson("nick", "keyword", "params", "1"));
    }

    @Test
    public void errorToJson() throws Exception {
        assertEquals("{\"type\":\"ERROR\",\"description\":\"nick\"}",
                ProtocolHelper.errorToJson("nick"));
    }

    @Test
    public void parseMessageFromUser() throws Exception {
        Gson gson = new Gson();


        JsonObject rootObject = new JsonObject();
        rootObject.addProperty("type", "REGISTRATION");
        rootObject.addProperty("nickname", "nick");

        RegistrationMessage res = (RegistrationMessage)
                ProtocolHelper.parseMessageFromUser(gson.toJson(rootObject));
        assertEquals(res.getType(), "REGISTRATION");
        assertEquals(res.getNickname(), "nick");


        rootObject = new JsonObject();
        rootObject.addProperty("type", "COMMAND");
        rootObject.addProperty("keyword", "nick");
        rootObject.addProperty("parameters", "params");

        CommandMessage cm = (CommandMessage)
                ProtocolHelper.parseMessageFromUser(gson.toJson(rootObject));
        assertEquals(cm.getKeyword(), "nick");
        assertEquals(cm.getParameters(), "params");



        rootObject = new JsonObject();
        rootObject.addProperty("type", "LOGOUT");

        LogOutMessage lom = (LogOutMessage)
                ProtocolHelper.parseMessageFromUser(gson.toJson(rootObject));


        rootObject = new JsonObject();
        rootObject.addProperty("type", "SIMPLE_MESSAGE");
        rootObject.addProperty("message", "aaa");

        UsualMessage um = (UsualMessage)
                ProtocolHelper.parseMessageFromUser(gson.toJson(rootObject));
        assertEquals(um.getMessage(), "aaa");

        rootObject = new JsonObject();
        rootObject.addProperty("type", "dadasda");

        AbstractMessage am =
                ProtocolHelper.parseMessageFromUser(gson.toJson(rootObject));
        assertEquals(am.getType(), "WITHOUT_TYPE");
    }

}