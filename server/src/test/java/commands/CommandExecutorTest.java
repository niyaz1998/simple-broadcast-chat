package commands;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by niyaz on 23.11.2018.
 */
public class CommandExecutorTest {
    @Test
    public void testPlugins() throws Exception {
        CommandExecutor executor = new CommandExecutor(
                "C:\\Users\\mvideo\\IdeaProjects\\Industrial Java Programming\\" +
                        "simple-broadcast-chat\\server\\test_plugins");
        System.out.println(executor.executeCommand("help", ""));
        assertEquals("10", executor.executeCommand("calc", "5+5"));
        assertEquals("plugin was not found", executor.executeCommand("really invalid command name", ""));
    }

}