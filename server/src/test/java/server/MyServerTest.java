package server;

import java.io.*;
import java.net.Socket;

/**
 * Created by niyaz on 10.11.2018.
 */
public class MyServerTest {
    @org.junit.Test
    public void start() throws Exception {
        final PipedOutputStream outputServerConsole = new PipedOutputStream();
        final PipedInputStream inputServerConsole  = new PipedInputStream(outputServerConsole);
        final MyServer myServer = new MyServer(8080, "stop", inputServerConsole);
        Thread serverThread = new Thread(new Runnable() {
            @Override
            public void run() {
                myServer.start();
            }
        });
        serverThread.start();

        Socket client = new Socket("localhost", 8080);
        DataOutputStream oos = new DataOutputStream(client.getOutputStream());
        oos.write("niyaz".getBytes());

        while (serverThread.isAlive()){}
    }

}