package runnables;

import managers.ClientsManager;
import managers.ConnectionManager;
import org.junit.Test;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.Socket;

import static org.junit.Assert.*;

/**
 * Created by niyaz on 12.11.2018.
 */
public class ChatRunnableTest {

    @Test
    public void run() throws Exception {
        final PipedOutputStream output = new PipedOutputStream();
        final PipedInputStream  input  = new PipedInputStream(output);
        ClientsManager clientsManager = new ClientsManager() {
            @Override
            public void acceptMessage(String newMessage, String address) {
                assertEquals("message", newMessage);
                System.out.println("successful");
            }

            @Override
            public void disconnect(String address) {

            }
        };
        ChatRunnable runnable = new ChatRunnable(input, "address", clientsManager);

        Thread thread = new Thread(runnable);
        thread.start();

        output.write("message\n".getBytes());
    }

}