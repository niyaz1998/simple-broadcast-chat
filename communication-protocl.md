# Type of messages
I choosed JSON as type of messages because it's simple and widespread

# Protocol:

## From user to server

### Registration or Log In
```
{
	"type": "REGISTRATION",
	"nickname": "nickname of new user, no restrictions for nickname"
}
```

### Log out (ubsupported for a while, or may be forever, who knows)
```
{
	"type": "LOGOUT"
}
```

### Usual Message
```
{
	"type": "SIMPLE_MESSAGE",
	"message": "content of message"
}
```

### Command
```
{
	"type": "COMMAND",
	"keyword": "command name ('help' for example)"
	"parameters": "command's parameters"
}
```

## From server to user(s)

### Registration message (from server to new user)
```
{
	"type": "REGISTRATION",
	"nickname": "nickname of a new user"
	"time": "time when new user was registered on server, in format 'yyyyMMdd'T'HHmmss'"
}
```

### New user message (from server to all users)
```
{
	"type": "NEW_USER",
	"nickname": "nickname of a new user"
	"time": "time when new user was registered on server, in format 'yyyyMMdd'T'HHmmss'"
}
```

### Log out user message (from server to all users)
```
{
	"type": "LOGOUT",
	"nickname": "nickname of leaving user",
	"time": "time when user leaved chat, in format 'yyyyMMdd'T'HHmmss'"
}
```

### Usual Message (from server to another users, except sender of message)
```
{
	"type": "SIMPLE_MESSAGE",
	"message": "content of message",
	"sender": "nickname of user who sent this message",
	"time": "time when this message was received on server side, in format 'yyyyMMdd'T'HHmmss'"
}
```

### Commands result (from server to sprecific user, who made a request)
```
{
	"type": "COMMAND",
	"keyword": "keyword",
	"parameters": "parameters",
	"result": "results of the command"
}
```

### ERROR (from server to specific user)
```
{
	"type": "ERROR",
	"description": "description of the error, may contains advices"
}
```