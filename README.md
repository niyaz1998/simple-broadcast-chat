# simple broadcast chat

Implementation of chat that broadcasts messages from users to everyone.
This chat supports nicknames, so at first user needs to enter his(her) nickname.
It also supports commands (ex: "/calc 5+5").
Communication between user and server is based on protocol described in communication-protocol.md file.

## Implementation
Implemented on Java language.
chat_user_interface - is project that allows to communicate with server.
plugins - contains implementations of plugins supported by server. 
server - implementation of server.

### Description of user's interface
I decided give user an ability to enter IP of server. Because this is project for study, and server doesn't have some stable IP.
So when interface starts user must enter some valid IP (or "localhost").
Then user needs to enter his nickname, so other users will know who's written message.
To use command user must enter '/' symbol, and then keyword of command and parameters of command separated by whitespace.

### Implementation of user's interface
Implementation based on sockets. When user enters his nickname new thread appears. This thread listens for messages. 
And if it can parse message due to protocol, message will be shown on screeen.

### Implementation of server
Implementation of chat is based on sockets.
Server keeps id's of every connection (id is an IPv4 address).
For every new received connection server starts new thread, that listens for messages from user.
When message is received it is processed by server due to protocol.

# Plugins
Server supports plugins. This allows server to be able dynamically add new commands to execute, or delete old ones.
Every plugin is an jar file, with class what implements specified interface.

##Interface
	getKeyword() - returns keyword of plugin, if two plugins will have same keywords, server will choose first one.
	getShortDescription() - returns description of plugins.
	executeCommand(String parameters) - accepts some string with parameters (provided by user), and returns result of execition of plugin.

## Implementation of plugins
api-plugin.jar contains CommandInterface.class file. This class describes what interface must be implemented by plugin.
So this jar file must be added to Java project as an library.
Then CommandInterface must be implemented by one or more classes in plugin.
And then project must be packed to jar and added to "server/plugins" folder.