
/**
 * represents abstract expression
 */
abstract public class Expression {
    /**
     * @return calculates result of expression
     */
    abstract long getResult();

    /**
     * @return JSON representation of an Expression
     */
    abstract String ToJSON();
}
