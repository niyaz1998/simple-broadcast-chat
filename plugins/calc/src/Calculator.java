import plugin.api.CommandInterface;

import java.text.ParseException;

/**
 * Created by niyaz on 23.11.2018.
 */
public class Calculator implements CommandInterface {
    @Override
    public String getKeyword() {
        return "calc";
    }

    @Override
    public String executeCommand(String s) {
        Parser parser = new Parser(s);
        try {
            return String.valueOf(parser.parse().getResult());
        } catch (ParseException e) {
            return e.getMessage();
        }
    }

    @Override
    public String getShortDescription() {
        return "calculates mathematical expressions"
                +"supports bracketed expressions and the following operators: " +
                "\"+\", \"-\", \"*\", \">\", \"<\", \"=\". " +
                "Comparison operators produce numerical representations of boolean values, " +
                "\"1\" for \"True\" and \"0\" for \"False\". ";
    }
}
