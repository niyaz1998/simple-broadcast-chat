/**
 * represents term of Factor and another term
 * can use operators such as:
 * +, -
 * if there is no operator, result of that Term expression will be result of right Expression
 */
public class Term extends Expression {
    enum Opcode {PLUS, MINUS, NONE}

    public Term(Opcode op, Expression result, Expression right) {
        this.op = op;
        this.left = result;
        this.right = right;
    }

    /**
     * @return result of the term
     */
    public long getResult(){
        switch (op){
            case NONE:
                return left.getResult();
            case PLUS:
                return left.getResult() + right.getResult();
            case MINUS:
                return left.getResult() - right.getResult();
            default:
                return left.getResult();
        }
    }

    @Override
    String ToJSON() {
        switch (op){
            case PLUS:
                return "{\"left\": "+left.ToJSON()+",\"operator\": \"PLUS\", \"right\": " + right.ToJSON()+"}";
            case MINUS:
                return "{\"left\": "+left.ToJSON()+",\"operator\": \"MINUS\", \"right\": " + right.ToJSON()+"}";
            default:
                return "{\"left\": "+left.ToJSON()+",\"operation\": \"NONE\", \"right\": \"NONE\"}";
        }
    }

    private Opcode op;
    private Expression left;
    private Expression right;
}
