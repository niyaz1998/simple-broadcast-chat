/**
 * represents Factor
 * can use operators such as:
 * multiplication and division (*,/)
 * if there is no operator, result of that Factor expression will be result of right Expression
 */
public class Factor extends Expression {
    enum Opcode {MULTIPLICATION, DIVISION, NONE}

    public Factor(Opcode op, Expression left, Expression right) {
        this.op = op;
        this.left = left;
        this.right = right;
    }

    public long getResult(){
        switch (op){
            case DIVISION:
                return left.getResult() / right.getResult();
            case MULTIPLICATION:
                return left.getResult() * right.getResult();
            default:
                return left.getResult();
        }
    }

    @Override
    String ToJSON() {
        switch (op){
            case DIVISION:
                return "{\"left\": "+left.ToJSON()+",\"operation\": \"DIVISION\", \"right\":" + right.ToJSON()+"}";
            case MULTIPLICATION:
                return "{\"left\": "+left.ToJSON()+",\"operation\": \"MULTIPLICATION\", \"right\":" + right.ToJSON()+"}";
            default:
                return "{\"left\": "+left.ToJSON()+",\"operation\": \"NONE\", \"right\": \"NONE\"}";
        }
    }

    private Opcode op;
    private Expression left;
    private Expression right;
}
