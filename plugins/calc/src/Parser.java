import java.text.ParseException;

/**
 * This class is a parser for mathematical expression
 * Creates expression tree
 */
public class Parser {
    private Expression root;
    private String input;
    private int count;//counter to current symbol

    /**
     * @return input that was given to the parser
     */
    public String getInput() {
        return input;
    }

    /**
     * constructor
     * @param input string to parse
     */
    public Parser(String input) {
        this.input = input;
        this.input = input.replaceAll(" ", "");
        count =0;
    }

    /**
     * parses given expression
     * @return expression
     * @throws ParseException if given string is incorrect, and cannot be parsed
     */
    public Expression parse() throws ParseException {
        if(input.length() == 0)
            throw new ParseException("Expression is empty", 0);
        root = parseRelation();
        return root;
    }

    /**
     * parses Relation part of the expression
     * @return Expression (Relation or something lower in hierarchy)
     * @throws ParseException if expression is incorrect
     */
    private Expression parseRelation() throws ParseException {
        Expression result = parseTerm();
        Relation.Opcode op = parseRelationOperator();
        if (op != Relation.Opcode.NONE) {
            Expression right = parseTerm();
            result = new Relation(op, result, right);
        }
        return result;
    }

    /**
     * parses Term part of the expression
     * @return Expression (Term or something lower in hierarchy)
     * @throws ParseException if expression is incorrect
     */
    private Expression parseTerm() throws ParseException {
        Expression result = parseFactor();
        while (true) {
            Term.Opcode op = parseTermOperator();
            if (op != Term.Opcode.NONE) {
                Expression right = parseFactor();
                result = new Term(op, result, right);
            } else {
                break;
            }
        }
        return result;
    }

    /**
     * parses Factor part of the expression
     * @return Expression (Factor or something lower in hierarchy)
     * @throws ParseException if expression is incorrect
     */
    private Expression parseFactor() throws ParseException {
        Expression result = parsePrimary();
        while (true) {
            Factor.Opcode op = parseFactorOperator();
            if (op != Factor.Opcode.NONE) {
                Expression right = parsePrimary();
                result = new Factor(op, result, right);
            } else {
                break;
            }
        }
        return result;
    }

    /**
     * parses primary expression at the current position
     * moves counter if needed
     * @return IntegerNumber if there is integer, or another expression, if there is parentheses
     * @throws ParseException if expression is incorrect
     */
    private Expression parsePrimary() throws ParseException {
        if ( Character.isDigit(input.charAt(count)) ||
                input.charAt(count)=='-' && Character.isDigit(input.charAt(count+1)))
            return parseInteger();
        else if ( input.charAt(count) == '(' ) {
            ++count;
            Parenthesized result = new Parenthesized(parseRelation());
            if((count>=input.length()-1) && input.charAt(count) != ')'){
                throw new ParseException("Bad parentheses, need closing bracket", count);
            }
            ++count;
            return result;
        } else {
            throw new ParseException("Incorrect Expression, unexpected symbol", count);
        }
    }

    /**
     * parses relation operator at the beginning of the rest
     * moves counter if needed
     * @return one of the relations operator (>,>=,<,<=,=,!=) or NONE if there is no relation operator
     * @throws ParseException if operator != written wrongly
     */
    private Relation.Opcode parseRelationOperator() throws ParseException {
        if(count>=input.length()-1) return Relation.Opcode.NONE;
        switch (input.charAt(count)) {
            case '>':
                count++;
                return Relation.Opcode.GREATER;
            case '<':
                count++;
                return Relation.Opcode.LESS;
            case '=':
                count++;
                return Relation.Opcode.EQUAL;
        }
        return Relation.Opcode.NONE;
    }

    /**
     * parses term operator at the current position
     * moves counter if needed
     * @return PLUS, MINUS or NONE if there is no term operator
     */
    private Term.Opcode parseTermOperator() {
        if(count>=input.length()-1) return Term.Opcode.NONE;
        switch(input.charAt(count)) {
            case '+':
                count++;
                return Term.Opcode.PLUS;
            case '-':
                count++;
                return Term.Opcode.MINUS;
            default:
                return Term.Opcode.NONE;
        }
    }

    /**
     * parses factor operator at current position
     * moves counter if needed
     * @return MULTIPLICATION, DIVISION or NONE if there is no factor operator
     */
    private Factor.Opcode parseFactorOperator() {
        if(count>=input.length()-1) return Factor.Opcode.NONE;
        switch(input.charAt(count)) {
            case '*':
                count++;
                return Factor.Opcode.MULTIPLICATION;
            case '/':
                count++;
                return Factor.Opcode.DIVISION;
            default:
                return  Factor.Opcode.NONE;
        }
    }

    /**
     * parses integer at the current position
     * moves counter if needed
     * @return IntegerNumber
     */
    private IntegerNumber parseInteger() {
        IntegerNumber result;
        StringBuilder strResult = new StringBuilder();
        if(input.charAt(count)=='-'){
            strResult.append(input.charAt(count));
            count++;
        }
        while(count<input.length() && Character.isDigit(input.charAt(count))){
            strResult.append(input.charAt(count));
            count++;
        }
        result = new IntegerNumber(Long.parseLong(strResult.toString()));
        return result;
    }
}
