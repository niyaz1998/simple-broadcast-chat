/**
 * represents relation for 2 numbers
 * can use operators such as:
 * >, >=, <, <=, =, !=
 * if there is no operator result of that Relation will be result of Term on the left side
 */
public class Relation extends Expression {

    enum Opcode {LESS, GREATER, EQUAL, NONE}

    /**
     * constructor for relation
     * @param op operation
     * @param left term on the right side
     * @param right term on the right side
     */
    public Relation(Opcode op, Expression left, Expression right) {
        this.op = op;
        this.left = left;
        this.right = right;
    }

    /**
     * returns result of relation
     * 1 if relation is true, 0 if relation is false
     * another number if there is no operation
     * @return result of relation
     */
    public long getResult() {
        switch (op){
            case LESS:
                return left.getResult() < right.getResult() ? 1 : 0;
            case EQUAL:
                return left.getResult() == right.getResult() ? 1 : 0;
            case GREATER:
                return left.getResult() > right.getResult() ? 1 : 0;
            default:
                return left.getResult();
        }
    }

    @Override
    String ToJSON() {
        switch (op){

            case LESS:
                return "{\"left\": "+left.ToJSON()+",\"operation\": \"LESS\", \"right\": " + right.ToJSON()+"}";
            case EQUAL:
                return "{\"left\": "+left.ToJSON()+",\"operation\": \"EQUAL\", \"right\": " + right.ToJSON()+"}";
            case GREATER:
                return "{\"left\": "+left.ToJSON()+",\"operation\": \"GREATER\", \"right\": " + right.ToJSON()+"}";
           default:
                return "{\"left\": "+left.ToJSON()+",\"operation\": \"NONE\", \"right\": \"NONE\"}";
        }
    }

    private Opcode op;
    private Expression left;
    private Expression right;
}
