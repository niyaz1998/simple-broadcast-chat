/**
 * represents Integer Number
 * contains value of type long
 */
public class IntegerNumber extends Expression {
    private long value;

    /**
     * constructor
     * @param value is a value to contain
     */
    public IntegerNumber(long value){
        this.value = value;
    }

    public long getResult(){
        return value;
    }

    @Override
    String ToJSON() {
        return "{\"value\": " + Long.toString(value) + "}";
    }
}
