public class Parenthesized extends Expression {

    private Expression inParentheses;

    public Parenthesized(Expression inParentheses) {
        this.inParentheses = inParentheses;
    }

    @Override
    long getResult() {
        return inParentheses.getResult();
    }

    @Override
    String ToJSON() {
        return inParentheses.ToJSON();
    }
}
