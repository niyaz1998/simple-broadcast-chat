import plugin.api.CommandInterface;

/**
 * Created by niyaz on 23.11.2018.
 */
public class Waiter implements CommandInterface {
    @Override
    public String getKeyword() {
        return "wait";
    }

    @Override
    public String executeCommand(String s) {
        synchronized (this) {
            try {
                wait(5000);
            } catch (InterruptedException e) {
                return e.getMessage();
            }
        }
        return "successfully waited";
    }

    @Override
    public String getShortDescription() {
        return "simply waits for 5 seconds and prints message";
    }
}
