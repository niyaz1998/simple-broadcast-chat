import config.ConfigProperties;
import config.ConfigReader;
import config.readers.ConsoleConfigReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.AbstractServerConnector;
import server.MyServer;

import java.io.IOException;

/**
 * Created by niyaz on 10.11.2018.
 */
public class Main {


    public static void main(String[] args) {
        ConfigProperties configProperties = readConfig();
        AbstractServerConnector myServer = new MyServer(configProperties, System.in, System.out);
        myServer.start();
    }

    private static ConfigProperties readConfig() {
        ConfigReader configReader = new ConsoleConfigReader();
        configReader.readProperties();
        return configReader.getProperties();
    }
}
