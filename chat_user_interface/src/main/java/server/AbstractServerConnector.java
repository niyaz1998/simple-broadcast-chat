package server;

import config.ConfigProperties;
import printers.DefaultPrinter;

import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;

/**
 * Created by niyaz on 23.11.2018.
 */
public abstract class AbstractServerConnector {
    protected InputStream readMessagesStream;
    protected PrintStream writeMessagesStream;
    protected ConfigProperties configProperties;
    protected String nickname = null;

    public AbstractServerConnector(ConfigProperties configProperties,
                    InputStream readMessagesStream,
                    PrintStream writeMessagesStream) {
        this.configProperties = configProperties;
        this.readMessagesStream = readMessagesStream;
        this.writeMessagesStream = writeMessagesStream;
    }

    public abstract void start();

    public abstract void acceptMessage(String message);

    abstract String getNickname();
}
