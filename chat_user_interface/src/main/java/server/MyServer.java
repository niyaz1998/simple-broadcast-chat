package server;

import config.ConfigProperties;
import helpers.ProtocolHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import printers.AbstractPrinter;
import printers.DefaultPrinter;
import runnables.ServerListener;

import java.io.*;
import java.net.Socket;
import java.util.Map;

/**
 * Created by niyaz on 10.11.2018.
 */
public class MyServer extends AbstractServerConnector{

    private final Logger logger = LogManager.getLogger(getClass().getName());
    private Socket socket;
    private String nickname = null;
    private DefaultPrinter printer;

    public MyServer(ConfigProperties configProperties,
                    InputStream readMessagesStream,
                    PrintStream writeMessagesStream) {
        super(configProperties, readMessagesStream, writeMessagesStream);
    }

    @Override
    public void start() {
        initSocket();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(readMessagesStream));
            printer = new DefaultPrinter(writeMessagesStream);
            initListener();

            writeMessagesStream.println("Enter your nickname: ");
            logger.info("started listening for messages");
            while (true) {
                String message = br.readLine();
                if (message.equalsIgnoreCase(configProperties.getKEYWORD_STOP())) { // stops server
                    logger.info("stopped listening for messages");
                    break;
                } else if (nickname != null) {
                    if (message.length() > 0 && message.charAt(0) == '/') { // if there is '/' symbol - send as command
                        sendMessageToServer(ProtocolHelper.commandToJson(message));
                    } else if (message.length() > 0) {
                        sendMessageToServer(ProtocolHelper.messageToJson(message));
                    }
                } else {
                    sendMessageToServer(ProtocolHelper.logInToJson(message));
                }
            }
            socket.close();
        } catch (IOException e) {
            logger.error("Reading from Input Stream failed in MyServer.start(): " + e.getMessage());
        }
    }

    /**
     * initializes thread to receive messages from server
     */
    private void initListener() {
        try {
            ServerListener runnable = new ServerListener(socket.getInputStream(), this);
            Thread messagesListener = new Thread(runnable);
            messagesListener.setDaemon(true);
            messagesListener.start();
        } catch (IOException e) {
            logger.error("failed initializing Input Stream for message listener: "
                    + e.getMessage());
        }
    }

    /**
     * initializes socket to listen from server
     */
    private void initSocket() {
        try {
            socket = new Socket(configProperties.getSERVER_ADDRESS(), configProperties.getSERVER_PORT());
        } catch (IOException e) {
            logger.error("error while opening a socket: " + e.getMessage());
        }
    }

    /**
     * sends message to server
     * @param message
     */
    private void sendMessageToServer(String message) {
        logger.info("sending message to server: " + message);
        try {
            DataOutputStream oos = new DataOutputStream(socket.getOutputStream());
            oos.write((message + "\n").getBytes());
        } catch (IOException e) {
            logger.error("IO exception: " + e.getMessage()
                    + " occurred while sending " + message);
        }
    }

    /**
     * processes message received from server
     * prints it actually if there is something to print
     * @param message
     */
    @Override
    public void acceptMessage(String message) {
        Map<String, String> parsedMessage = ProtocolHelper.parseMessageFromServer(message);

        if (parsedMessage.get(ProtocolHelper.TYPE) == null)
            return;
        if (parsedMessage.get(ProtocolHelper.TYPE).equals(ProtocolHelper.TYPE_REGISTRATION)) {
            if (parsedMessage.get(ProtocolHelper.NICKNAME) != null) {
                nickname = parsedMessage.get(ProtocolHelper.NICKNAME);
                writeMessagesStream.println("You was successfully registered on server");
            }
        }
        printer.printMessage(parsedMessage);
    }

    @Override
    String getNickname() {
        return nickname;
    }
}
