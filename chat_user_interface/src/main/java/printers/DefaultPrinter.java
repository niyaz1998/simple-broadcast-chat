package printers;

import helpers.ProtocolHelper;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created by niyaz on 12.11.2018.
 * prints messages to given PrintStream
 */
public class DefaultPrinter extends AbstractPrinter{

    private final PrintStream writer;

    public DefaultPrinter(PrintStream writer) {
        this.writer = writer;
    }

    @Override
    public void printMessage(Map<String, String> message) {
        String type = message.get(ProtocolHelper.TYPE);
        if (type == null || type.length() == 0){
            return;
        }
        switch (type){
            case ProtocolHelper.TYPE_NEW_USER:
                writer.println("[" + message.get(ProtocolHelper.TIME) + "] "
                        +  message.get(ProtocolHelper.NICKNAME) + " joined the chat!");
                break;
            case ProtocolHelper.TYPE_SIMPLE_MESSAGE:
                writer.println("[" + message.get(ProtocolHelper.TIME) + "] "
                        +  message.get(ProtocolHelper.SENDER) + " says: " + message.get(ProtocolHelper.MESSAGE));
                break;
            case ProtocolHelper.TYPE_LOGOUT:
                writer.println("[" + message.get(ProtocolHelper.TIME) + "] "
                        +  message.get(ProtocolHelper.NICKNAME) + " leaves the chat!");
                break;
            case ProtocolHelper.TYPE_COMMAND:
                writer.println("[" + message.get(ProtocolHelper.TIME) + "]:"
                        +  message.get(ProtocolHelper.KEYWORD)
                        + " " + message.get(ProtocolHelper.PARAMETERS) + " executed!\n"
                        + "results: \n" + message.get(ProtocolHelper.RESULT)
                        + "\nend");
                break;
            case ProtocolHelper.TYPE_ERROR:
                writer.println("[error occurred] "
                        +  message.get(ProtocolHelper.DESCRIPTION));
                break;
        }
    }
}
