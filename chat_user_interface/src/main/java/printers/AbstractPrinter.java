package printers;

import java.util.Map;

/**
 * Created by niyaz on 12.11.2018.
 * responsible for printing messages to user
 */
public abstract class AbstractPrinter {

    public abstract void printMessage(Map<String, String> message);
}
