package runnables;

import com.google.gson.Gson;
import helpers.ProtocolHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import printers.DefaultPrinter;
import server.AbstractServerConnector;
import server.MyServer;

import java.io.*;

/**
 * Created by niyaz on 12.11.2018.
 * Listens for messages from server
 * parses messages and sends them to MyServer
 */
public class ServerListener implements Runnable {

    private final Logger logger = LogManager.getLogger(getClass().getName());
    private static final Gson gson = new Gson();
    private AbstractServerConnector myServer;
    private InputStream input;

    public ServerListener(InputStream input, AbstractServerConnector myServer) {
        this.input = input;
        this.myServer = myServer;
    }

    @Override
    public void run() {
        BufferedReader in = null;
        try {
             in  = new BufferedReader(new InputStreamReader(input));

            StringBuilder message = new StringBuilder();
            String newLine;
            while ((newLine = in.readLine()) != null) {
                message.append(newLine);
                if (isJSONValid(message.toString())) {
                    myServer.acceptMessage(message.toString());
                    message = new StringBuilder();
                }
            }
        } catch (IOException e) {
            try {
                in.close();
            } catch (IOException e1) {
                logger.error("ServerListener.run(): " + e.getMessage());
            }
            if (!e.getMessage().equals("Socket closed"))
                logger.error("ServerListener.run(): " + e.getMessage());
        }
    }

    /**
     * checks if given string is valid JSON
     * @param jsonInString
     * @return
     */
    private static boolean isJSONValid(String jsonInString) {
        try {
            gson.fromJson(jsonInString, Object.class);
            return true;
        } catch(com.google.gson.JsonSyntaxException ex) {
            return false;
        }
    }
}
