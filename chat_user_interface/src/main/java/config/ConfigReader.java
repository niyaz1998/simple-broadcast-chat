package config;

/**
 * Created by niyaz on 10.11.2018.
 */
public abstract class ConfigReader {

    public abstract ConfigProperties getProperties();

    public abstract void readProperties();
}
