package config.readers;

import config.ConfigProperties;
import config.ConfigReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by niyaz on 10.11.2018.
 * responsible for reading cnfig from the console
 */
public class ConsoleConfigReader extends ConfigReader{

    private final Logger logger = LogManager.getLogger(getClass().getName());
    private ConfigProperties properties =
            new ConfigProperties(null, 8080, "exit()");


    public ConfigProperties getProperties() {
        return properties;
    }

    public void readProperties() {
        Scanner input = new Scanner(System.in);
        System.out.println("Default settings for client are: ");
        System.out.println(properties.generateReportString());
        System.out.println("Please enter server address: ");

        properties.setSERVER_ADDRESS(input.nextLine());
        logger.info("Config is successfully read");
        logger.info("\n" + properties.generateReportString());
    }
}
