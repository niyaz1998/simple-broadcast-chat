package config;

/**
 * Created by niyaz on 10.11.2018.
 * represents configuration of user's client
 */
public class ConfigProperties {

    private String SERVER_ADDRESS;
    private int SERVER_PORT = 0;
    private String KEYWORD_STOP;

    public ConfigProperties(String SERVER_ADDRESS, int SERVER_PORT, String KEYWORD_STOP) {
        this.SERVER_ADDRESS = SERVER_ADDRESS;
        this.SERVER_PORT = SERVER_PORT;
        this.KEYWORD_STOP = KEYWORD_STOP;
    }

    public String getSERVER_ADDRESS() {
        return SERVER_ADDRESS;
    }

    public void setSERVER_ADDRESS(String SERVER_ADDRESS) {
        this.SERVER_ADDRESS = SERVER_ADDRESS;
    }

    public int getSERVER_PORT() {
        return SERVER_PORT;
    }

    public void setSERVER_PORT(int SERVER_PORT) {
        this.SERVER_PORT = SERVER_PORT;
    }

    public String getKEYWORD_STOP() {
        return KEYWORD_STOP;
    }

    public void setKEYWORD_STOP(String KEYWORD_STOP) {
        this.KEYWORD_STOP = KEYWORD_STOP;
    }

    public String generateReportString(){
        StringBuilder str = new StringBuilder();
        if (SERVER_ADDRESS != null) {
            str.append("server address is: ").append(SERVER_ADDRESS).append("\n");
        }
        if (SERVER_PORT != 0){
            str.append("server port is: ").append(SERVER_PORT).append("\n");
        }
        if (KEYWORD_STOP != null){
            str.append("stopping keyword is: ").append(KEYWORD_STOP).append("\n");
        }
        return str.toString();
    }
}
