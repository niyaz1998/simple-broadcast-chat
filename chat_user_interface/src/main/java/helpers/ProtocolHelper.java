package helpers;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Class helps to transform strings into messages according to protocol
 * you can find protocol in directory "simple-broadcast-chat"
 *
 */
public class ProtocolHelper {


    public static final String TYPE = "type"; // field included in every message
    public static final String TYPE_REGISTRATION = "REGISTRATION";
    public static final String TYPE_LOGOUT = "LOGOUT";
    public static final String TYPE_COMMAND = "COMMAND";
    public static final String TYPE_SIMPLE_MESSAGE = "SIMPLE_MESSAGE";
    public static final String TYPE_UNSUPPORTED = "UNSUPPORTED_TYPE";
    public static final String TYPE_WITHOUT_TYPE = "WITHOUT_TYPE";
    public static final String TYPE_ERROR = "ERROR";
    public static final String TYPE_NEW_USER = "NEW_USER";

    public static final String NICKNAME = "nickname";
    public static final String MESSAGE = "message";
    public static final String KEYWORD = "keyword";
    public static final String PARAMETERS = "parameters";
    public static final String SENDER = "sender";
    public static final String TIME = "time";
    public static final String RESULT = "result";
    public static final String DESCRIPTION = "description";


    private static final Gson gson = new Gson();

    public static String logInToJson(String nickname){
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty(TYPE, TYPE_REGISTRATION);
        rootObject.addProperty(NICKNAME, nickname);

        return gson.toJson(rootObject);
    }

    public static String logOutToJson(){
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty(TYPE, TYPE_LOGOUT);
        return gson.toJson(rootObject);
    }

    public static String messageToJson(String content){
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty(TYPE, TYPE_SIMPLE_MESSAGE);
        rootObject.addProperty(MESSAGE, content);

        return gson.toJson(rootObject);
    }

    public static String commandToJson(String input){
        StringBuilder keyword = new StringBuilder();
        int i = 1;
        char c = input.charAt(i);
        while(c != ' ') {
            keyword.append(c);
            i++;
            if(i == input.length())
                break;
            c = input.charAt(i);
        }
        if(i== input.length())
            return commandToJson(keyword.toString(), "");
        return commandToJson(keyword.toString(), input.substring(i+1));
    }

    public static String commandToJson(String keyword, String parameters){
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty(TYPE, TYPE_COMMAND);
        rootObject.addProperty(KEYWORD, keyword);
        rootObject.addProperty(PARAMETERS, parameters);

        return gson.toJson(rootObject);
    }

    /**
     * parses json messages from the server according to the protocol
     * @param json JSON string to parse
     * @return map of keywords and values     *
     */
    public static Map<String, String> parseMessageFromServer(String json){
        Type mapType = new TypeToken<Map>(){}.getType();
        return gson.fromJson(json, mapType);
    }
}
