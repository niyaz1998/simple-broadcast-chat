package config.readers;

import config.ConfigProperties;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Created by niyaz on 10.11.2018.
 */
public class ConsoleConfigReaderTest {
    @Test
    public void getProperties() throws Exception {
        final ConsoleConfigReader reader = new ConsoleConfigReader();
        InputStream testInput = new ByteArrayInputStream( "localhost".getBytes("UTF-8"));
        System.setIn( testInput );
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                reader.readProperties();
            }
        });
        thread.start();
        while (thread.isAlive()){}
        assertEquals(reader.getProperties().getSERVER_ADDRESS(), "localhost");
    }

}