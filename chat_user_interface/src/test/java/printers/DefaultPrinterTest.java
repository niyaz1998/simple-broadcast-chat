package printers;

import helpers.ProtocolHelper;
import org.junit.Test;

import java.io.*;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by niyaz on 23.11.2018.
 */
public class DefaultPrinterTest {
    @Test
    public void printMessage() throws Exception {

        final PipedOutputStream output = new PipedOutputStream();
        final BufferedReader input  = new BufferedReader(new InputStreamReader(new PipedInputStream(output)));
        DefaultPrinter printer = new DefaultPrinter(new PrintStream(output));

        HashMap<String, String> message = new HashMap<>();
        message.put(ProtocolHelper.TYPE, ProtocolHelper.TYPE_NEW_USER);
        message.put(ProtocolHelper.NICKNAME, "nick");
        printer.printMessage(message);
        assertEquals("[null] nick joined the chat!", input.readLine());

        message = new HashMap<>();
        message.put(ProtocolHelper.TYPE, ProtocolHelper.TYPE_SIMPLE_MESSAGE);
        message.put(ProtocolHelper.SENDER, "nick");
        message.put(ProtocolHelper.MESSAGE, "message");
        printer.printMessage(message);
        assertEquals("[null] nick says: message", input.readLine());

        message = new HashMap<>();
        message.put(ProtocolHelper.TYPE, ProtocolHelper.TYPE_LOGOUT);
        message.put(ProtocolHelper.NICKNAME, "nick");
        printer.printMessage(message);
        assertEquals("[null] nick leaves the chat!", input.readLine());

        message = new HashMap<>();
        message.put(ProtocolHelper.TYPE, ProtocolHelper.TYPE_COMMAND);
        message.put(ProtocolHelper.KEYWORD, "keyword");
        message.put(ProtocolHelper.PARAMETERS, "parameters");
        message.put(ProtocolHelper.RESULT, "result");
        printer.printMessage(message);
        assertEquals("[null]:keyword parameters executed!", input.readLine());
        assertEquals("results: ", input.readLine());
        assertEquals("result", input.readLine());
        assertEquals("end", input.readLine());

        message = new HashMap<>();
        message.put(ProtocolHelper.TYPE, ProtocolHelper.TYPE_ERROR);
        message.put(ProtocolHelper.DESCRIPTION, "description");
        printer.printMessage(message);
        assertEquals("[error occurred] description", input.readLine());
    }

}