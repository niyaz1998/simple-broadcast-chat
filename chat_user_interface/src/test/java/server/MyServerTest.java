package server;

import config.ConfigProperties;
import org.junit.Test;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import static org.junit.Assert.*;

/**
 * Created by niyaz on 10.11.2018.
 */
public class MyServerTest {

    @Test
    public void start() throws Exception {
        // initialization
        final ServerSocket serverSocket = new ServerSocket(8083);

        Thread threadServer = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Socket clientsServerSocket = serverSocket.accept();
                    BufferedReader inServer  = new
                            BufferedReader(new
                            InputStreamReader(clientsServerSocket.getInputStream()));
                    DataOutputStream oos = new DataOutputStream(clientsServerSocket.getOutputStream());


                    assertEquals(
                            "{\"type\":\"REGISTRATION\",\"nickname\":\"nick\"}",
                            inServer.readLine());
                    System.out.println("sending registration approval");
                    oos.write("{\"type\":\"REGISTRATION\",\"nickname\":\"nick\"}\n".getBytes());

                    assertEquals(
                            "{\"type\":\"SIMPLE_MESSAGE\",\"message\":\"some message\"}",
                            inServer.readLine());
                    oos.write(("{\"type\":\"SIMPLE_MESSAGE\",\"message\":\"some message\"" +
                            ",\"sender\":\"sender\",\"time\":\"time\"}\n").getBytes());

                    serverSocket.close();
                    clientsServerSocket.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        threadServer.start();

        // for My server
        ConfigProperties configProperties =
                new ConfigProperties("localhost", 8083, "stop");
        final PipedOutputStream outputClientConsole = new PipedOutputStream();
        final PipedInputStream inputClientConsole  = new PipedInputStream(outputClientConsole);
        final MyServer server = new MyServer(configProperties, inputClientConsole, System.out);

        Thread threadClient = new Thread(new Runnable() {
            @Override
            public void run() {
                server.start();
            }
        });
        threadClient.start();

        outputClientConsole.write("nick\n".getBytes());
        while (server.getNickname() == null){Thread.yield();}
        outputClientConsole.write("some message\n".getBytes());
        outputClientConsole.write("stop\n".getBytes());
        threadClient.join();
        assertFalse(threadClient.isAlive());
        outputClientConsole.close();
        inputClientConsole.close();
        serverSocket.close();
    }


}