package helpers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by niyaz on 12.11.2018.
 */
public class ProtocolHelperTest {
    @Test
    public void logInToJson() throws Exception {
        assertEquals("{\"type\":\"REGISTRATION\",\"nickname\":\"nick\"}",
                ProtocolHelper.logInToJson("nick"));
    }

    @Test
    public void logOutToJson() throws Exception {
        assertEquals("{\"type\":\"LOGOUT\"}",
                ProtocolHelper.logOutToJson());
    }

    @Test
    public void messageToJson() throws Exception {
        assertEquals("{\"type\":\"SIMPLE_MESSAGE\",\"message\":\"content of message\"}",
                ProtocolHelper.messageToJson("content of message"));
    }

    @Test
    public void commandToJson() throws Exception {
        assertEquals("{\"type\":\"COMMAND\",\"keyword\":\"command\",\"parameters\":\"params\"}",
                ProtocolHelper.commandToJson("command", "params"));


        assertEquals("{\"type\":\"COMMAND\",\"keyword\":\"command\",\"parameters\":\"params\"}",
                ProtocolHelper.commandToJson("/command params"));
    }

    @Test
    public void parseMessageFromServer() throws Exception {
        Gson gson = new Gson();


        JsonObject rootObject = new JsonObject();
        rootObject.addProperty("type", "USER_LOGIN");
        rootObject.addProperty("nickname", "nick");

        Map<String, String> res = ProtocolHelper.parseMessageFromServer(gson.toJson(rootObject));
        assertEquals(res.get("type"), "USER_LOGIN");
        assertEquals(res.get("nickname"), "nick");
    }

}